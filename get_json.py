import requests


def config():
  response = requests.get("https://ifconfig.co/json")
  data={}

  if response.status_code == 200:
    try:
      data = response.json()
    except:
      pass

  return data


print(config())